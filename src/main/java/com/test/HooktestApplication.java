package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HooktestApplication {

	public static void main(String[] args) {
		SpringApplication.run(HooktestApplication.class, args);
	}
}
